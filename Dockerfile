FROM python:3.7-slim

RUN mkdir /app

WORKDIR /app

COPY app.py /app
COPY requirenments.txt /app

RUN pip install -r /app/requirenments.txt

EXPOSE  8000

ENTRYPOINT ["python", "-u", "app.py"]