#!/usr/bin/env python3.6

import os
import time
import boto3
import logging
from datetime import datetime, timedelta
from prometheus_client import start_http_server, Metric, REGISTRY


logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s:%(name)s:%(levelname)s:%(message)s'
)

engine_metrics = {
    'aurora': [{'Metric':'db.SQL.Innodb_rows_read.avg'}],
    'aurora-postgresql': [{'Metric':'db.transactions.xact_commit.avg'}],
    'postgres': [{'Metric': 'db.transactions.xact_commit.avg' }],
    'aurora-mysql': [
        {
            'Metric': 'db.load.avg',
            'GroupBy': {
                'Group': 'db.sql',
                'Dimensions': [
                    'db.sql.statement'
                ],
            },
        },
        {
            'Metric': 'db.load.avg',
            'GroupBy': {
                'Group': 'db.user',
                'Dimensions': [
                    'db.user.name',
                ],
            },
        },
    ]
}

def get_pi_instances():
    return filter(
        lambda _: _.get('PerformanceInsightsEnabled', False),
        session.client('rds').describe_db_instances()['DBInstances']
    )

def get_resource_metrics(instance):

    metric_queries = []

    if engine_metrics.get(instance['Engine'], False):
        for metric in engine_metrics[instance['Engine']]:
            metric_queries.append(metric)

    if not metric_queries:
        return

    return session.client('pi').get_resource_metrics(
        ServiceType='RDS',
        Identifier=instance['DbiResourceId'],
        StartTime=time.time() - 300,
        EndTime=time.time(),
        PeriodInSeconds=60,
        MetricQueries=metric_queries
    )


class MetricCollector(object):

    def __init__(self):
        pass

    def collect(self):

        for instance in get_pi_instances():
            for metricList in [x for x in get_resource_metrics(instance)['MetricList'] if 'Dimensions' in x['Key']]:
                for dimension_name in metricList['Key']['Dimensions']:
                    metric_name = "pi_%s" % dimension_name.replace(".","_")

                    for dataPoint in metricList['DataPoints']:
                        metric = Metric(
                            metric_name,
                            "RDS PI %s %s" % (metric_name, instance['DBInstanceIdentifier']), "gauge"
                        )
                        metric.add_sample(
                             name = metric_name,
                             value = float(
                                 dataPoint['Value']
                             ),
                             labels = {
                                 dimension_name.replace(".", "_"): " ".join(
                                     metricList['Key']['Dimensions'][dimension_name].split()
                                 ),
                                 "instance": instance['DBInstanceIdentifier']
                             },
                             timestamp = float(
                                 datetime.timestamp(dataPoint['Timestamp'])
                             )
                        )
                        yield metric

                    logging.info("%s for instance %s updated." % (
                        metric_name, instance['DBInstanceIdentifier'])
                    )


if __name__ == '__main__':
    session = boto3.session.Session(
        region_name=os.environ.get('AWS_DEFAULT_REGION', 'eu-central-1'),
        aws_access_key_id=os.environ.get('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=os.environ.get('AWS_SECRET_ACCESS_KEY')
    )

    start_http_server(8000)

    REGISTRY.register(MetricCollector())

    while True: time.sleep(1)


